#!/bin/sh

# This can be change according to the name of the branch and commit message
name="TestBranch"
message="commit"

# This can be commented out if you use constant branch name and message
read -p "Enter Branch Name: " $name
read -p "Enter Commit Message: " $message

git branch -M $name
git add -A
git commit -m $message
git push -u origin $name
