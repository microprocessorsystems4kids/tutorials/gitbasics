#!/bin/sh

# This can be change according to the name of the branch
name="TestBranch"

# This can be commented out if you use constant branch name
read -p "Enter Branch Name merging to main: " $name

git checkout main
git pull
git checkout $name
git merge main
git push -u origin $name
