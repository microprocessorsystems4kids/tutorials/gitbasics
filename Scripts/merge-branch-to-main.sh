#!/bin/sh

# This can be change according to the name of the branch
name="TestBranch"

# This can be commented out if you use constant branch name
read -p "Enter Branch Name merging to main: " $name

git checkout main
git pull
git merge $name
git push -u origin main
