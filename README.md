## WELCOME TO Git Basics

These are hyperlinks to other tutorials

here for [basic tutorials][basictutorials]

here for [games tutorials][gametutorials]


[basictutorials]: https://gitlab.com/microprocessorsystems4kids/tutorials/basictutorials/
[gametutorials]: https://gitlab.com/microprocessorsystems4kids/tutorials/embeddedsystems/gamedevelopment/

#
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**
- [Introduction](#introduction)
- [Basic](#basic)
- [Online Tutorials](#online-tutorials)
<!-- END doctoc generated TOC please keep comment here to allow auto update -->
#

## Introduction

All the Git hosting services such as GitLab, GitHub, Bitbucket, etc. use the same git service originally developed by Linus Torvalds to keep track of developing kernel as open-source software.

![LinusTovarlds](images/linus.jpg){height=400px width=400px}

The main reason for using GitLab for the Microprocessor Systems Projects instead of GitHub is that GitHub is owned by Microsoft; meanwhile GitLab is owned by open-core company called GitLab Inc.


## Basic
1. For each member of the group is required to request access as show in the screenshot

![request](images/request.png){height=200px width=800px}

#
2. These Commands are used to configure user and email of the gitlab account

```
git config --global user.name "polsl_student" # Here you provide real username inside the quotes
```

```
git config --global user.email "student@polsl.pl" # Here you provide real email inside the quotes
```

#
3. The Command To clone (Download) the git repository (group one is used as an example)

```
git clone https://gitlab.com/microprocessorsystems4kids/year2023/groupone.git
```

Since you are outside of the cloned git directory, you must go into the directory

```
cd groupone # go to the cloned git directory
```

Now you can check status of the branch of the repository

```
git status # gives information on what branch you are on
```

The result is supposed to be

```
On branch main
Your branch is up to date with 'origin/main'.

nothing to commit, working tree clean
```

The Screenshot below shows how to obtain the HTTPS of the git repository

![clone](images/clone.png){height=200px width=800px}

#
4. The Command to creating new branches, switching between them 

The following command allows us to create new branch and automatically checkouts (switches) to it

```
git branch -M TestBranch # -M automatically checkouts to TestBranch
```

Now you can check status of the new branch of the repository

```
git status # gives information on what branch you are on
```

The result is supposed to be

```
On branch TestBranch
Your branch is up to date with 'origin/main'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   README.md

no changes added to commit (use "git add" and/or "git commit -a")
```

Now you can switch between main and TestBranch with the following command

```
git checkout main # checkouts to branch called "main"
```

to delete branch which is locally on your computer

```
git branch -D TestBranch # you have to checkout other branch to delete a given branch 
```

This allows to reset to the last commit, but you can also reset to any given commit

```
git reset --hard HEAD # specific commit number instead of HEAD if want to reset to older commit
```

#
5. The Commands to Add, Commit and Push a Branch 

```
git add -A # add all new files to the branch
```

```
git commit -m "useful features" # commits changes made on the branch
```

```
git push -u origin TestBranch # pushes the updated branch to the gitlab
```

#
6. The Command to merge branches to main branch

This process used to update changes of other branches to the main branch

the command checkouts (switches) to the main branch.

```
git checkout main # checkouts to branch called "main"
```

the following command is used to update any changes made on remote branch (on gitlab) in the local branch (on PC)

```
git pull # gets the newest changes from the "main" branch
```

when the local branch is upto date then you can merge the branches

```
git merge TestBranch # merge TestBranch into main
```

```
git push -u origin main # pushes the updated branch to the gitlab
```

#
7. The Command to merge main branch to branches. 

This process used to update changes in other branches which have taken place in the main branch

the command checkouts (switches) to the main branch.

```
git checkout main # checkouts to branch called "main"
```

the following command is used to update any changes made on remote branch (on gitlab) in the local branch (on PC)

```
git pull # gets the newest changes from the "main" branch
```

the command checkouts (switches) to the TestBranch branch.

```
git checkout TestBranch # checkouts to branch called "TestBranch"
```

when the local main branch is upto date then you can merge main to TestBranch

```
git merge main # merge main into TestBranch
```

```
git push -u origin TestBranch # pushes the updated branch to the gitlab
```
#

The following diagram represents the main idea of merging to and from main branch.
As it can be noticed Branch B is able to get the changes made by Branch A in the main branch
and vice versa. 

![git](images/git.png){height=300px width=800px}

Additionally, the folder called "Scripts" consists of four useful scripts.

#
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**
- [Introduction](#introduction)
- [Basic](#basic)
- [Online Tutorials](#online-tutorials)
<!-- END doctoc generated TOC please keep comment here to allow auto update -->
#

## Online Tutorials

It is natural to find some tutorials or examples good and others bad.
There is nothing wrong in using online [video tutorials][videos].
Additionally, there are [git cheatsheet][cheatsheet] and [visual cheatsheet][visualcheatsheet].
Remember to also look at the [reference documentation][reference].
Furthermore, there are many [GUI Clients][gui] for students who prefer using a GUI application instead of the terminal.

[videos]: https://git-scm.com/videos
[cheatsheet]: https://training.github.com/downloads/github-git-cheat-sheet/
[visualcheatsheet]: https://ndpsoftware.com/git-cheatsheet.html#loc=local_repo;
[reference]: https://git-scm.com/docs
[gui]: https://git-scm.com/downloads/guis

#
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**
- [Introduction](#introduction)
- [Basic](#basic)
- [Online Tutorials](#online-tutorials)
<!-- END doctoc generated TOC please keep comment here to allow auto update -->
#

These are hyperlinks to other tutorials

here for [basic tutorials][basictutorials]

here for [games tutorials][gametutorials]

[basictutorials]: https://gitlab.com/microprocessorsystems4kids/tutorials/basictutorials/
[gametutorials]: https://gitlab.com/microprocessorsystems4kids/tutorials/embeddedsystems/gamedevelopment/
